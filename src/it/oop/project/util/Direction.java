package it.oop.project.util;

/**
 * Enum listing possible directions in 2048.
 *
 */
public enum Direction {
    UP, RIGHT, DOWN, LEFT;
}
